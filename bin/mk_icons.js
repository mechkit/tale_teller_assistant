#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

const icon_names = {
  'abandoned': 'crypt-entrance',
  'all': 'all-seeing-eye',
  'arrow_left_arrow': 'arrow-left-right',
  'backlink': 'harpoon-chain',
  'box_arrow_right': 'box-arrow-right',
  'building': 'bx-building-house',
  'building.animal_yard': 'cow',
  'building.appiary': 'beehive',
  'building.baker': 'dough-roller',
  'building.blacksmith': 'anvil-impact',
  'building.bookbinder': 'book-cover',
  'building.butcher': 'meat',
  'building.carpenter': 'wood-beam',
  'building.chandlers': 'candle-holder',
  'building.farm': 'plant-roots',
  'building.furrier': 'animal-hide',
  'building.inn': 'bed',
  'building.jeweler': 'necklace',
  'building.leather tanner': 'animal-hide',
  'building.mill': 'stone-wheel',
  'building.mine': 'miner',
  'building.paper_maker': 'papers',
  'building.refinery': 'fireplace',
  'building.sawmill': 'wood-beam',
  'building.shoemaker': 'boots',
  'building.smithy': 'anvil-impact',
  'building.spice_market': 'powder-bag',
  'building.spice_mixer': 'powder-bag',
  'building.stable': 'stable',
  'building.store': 'storefront',
  'building.store-armor': 'leather-armor',
  'building.store-book': 'bookshelf',
  'building.store-farm': 'barn',
  'building.store-food': 'camp-cooking-pot',
  'building.store-general': 'storefront',
  'building.store-item': 'chest',
  'building.store-potion': 'potion-ball',
  'building.store-shoe': 'boots',
  'building.store-weapon': 'axe-sword',
  'building.string_maker': 'sewing-string',
  'building.tailor': 'shirt',
  'building.town_hall': 'office-building',
  'building.vineyard': 'vine-leaf',
  'building.weaver': 'rolled-cloth',
  'building.wine_cellar': 'cellar-barrels',
  'building.winery': 'wine-bottle',
  'building.wood yard': 'log',
  'card_close': 'picture-in-picture-exit-fill',
  'card_open': 'dungeon-gate',
  'card_reload': 'reload',
  'card_save': 'box-arrow-in-right',
  'card_tree': 'tree',
  'card_tree_parent': 'tree_parent',
  'card_tree_children': 'tree_children',
  'category': 'open-folder',
  'character': 'person-fill',
  'character.adventurer': 'backpack',
  'character.townfolk': 'person-fill',
  'city': 'family-house',
  'city.hamlet': 'huts-village',
  'city.metropolis': 'capitol',
  'city.town': 'block-house',
  'city.village': 'village',
  'civilization': 'trumpet-flag',
  'connection': 'arrows-exchange',
  'deck': 'library-books',
  'discard': 'bx-trash',
  'filter': 'bx-filter-alt',
  'flour mill': 'flour',
  'fort': 'hill-fort',
  'general_store': 'storefront',
  'generic': 'new-file',
  'group': 'grid',
  'group.district': 'pie-chart-2-line',
  'help': 'help-circle',
  'loot.armor': 'leather-armor',
  'loot.item': 'chest',
  'loot.melee': 'axe-sword',
  'loot.ranged': 'bow-arrow',
  'menu': 'menu',
  'new_card': 'new-file',
  'of': 'grid',
  'place': 'place',
  'playing_cards': 'card-queen-hearts',
  'quirk': 'swap-bag',
  'randomize': 'dice-six-faces-five',
  'reference': 'bookmarklet',
  'relationship': 'make-group',
  'sample': 'grid-3x3-gap-fill',
  'sample_pick': 'card-pickup',
  'sidebar_close': 'menu-fold',
  'sidebar_open': 'menu-unfold',
  'task': 'card-checklist',
  'tools': 'toolbox',
  'unknown': 'question-octagon-fill',
  'void': 'empty-hourglass',
  'world': 'world',
  'world_goto': 'world_reentry',
  'world_destroy': 'exploding-planet',
  'TTA': 'TTA-icon',
  //'random_card': 'card-random',
};

const prenamed_icons = Object.values(icon_names);

const icon_dir = './src/icons/';

// some helpful functions
const is_svg = file => /.svg$/.test(file);
const remove_extension = file => file.split('.')[0];
const toPascalCase = string =>
  string
    .match(/[a-z]+/gi)
    //.split(/[_-]/gi)
    .map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase())
    .join('');

// getting all the icons
const icons = fs.readdirSync(icon_dir)
  .filter(is_svg)
  .map(remove_extension);

// auto add icon files that are not in the table above
icons.forEach( icon_file_name => {
  if ( ! prenamed_icons.includes( icon_file_name ) ) {
    icon_names[icon_file_name] = icon_file_name;
  }
});

///////////////////////////////////
// WEB

let web = {
  build_path: './src/components/',
  file_name: 'Icon.jsx',
  import_list: [],
  Icon_lines: [],
};

web.Icon_lines.push("import styled from 'styled-components';");
web.Icon_lines.push('');

web.Icon_lines = web.Icon_lines.concat( icons.map( icon => `import { ReactComponent as ${toPascalCase(icon)} } from '../icons/${icon}.svg';`) );
web.Icon_lines.push('');

console.log( icons )

web.Icon_lines.push('const icon_map = {');
web.Icon_lines = web.Icon_lines.concat(
  Object.keys(icon_names).map(icon_name => {
    let file_name = icon_names[icon_name];
    if ( icons.includes( file_name ) ) {
      return `  '${icon_name}': ${toPascalCase(file_name)},`;
    } else {
      return `  //'${icon_name}': ${toPascalCase(file_name)},`;
    }
  }).join('\n')
)
web.Icon_lines.push('};');

web.Icon_lines = web.Icon_lines.concat([
  "",
  "export const icon_names = Object.keys(icon_map);",
  "",
  "const IconStr = styled.span`",
  /*eslint no-template-curly-in-string: "off"*/
  "  color: ${props=>props.color};",
  "  font-size: ${props=>props.size};",
  "`",
  "",
  "export default function Icon( { type, size, color, icon, ...rest } ) {",
  "  size = size || '1em';",
  "  color = color || 'black';",
  "  if ( icon ) {",
  "    return <IconStr color={color} size={size} >{icon}</IconStr>",
  "  } else {",
  "    const Icon = icon_map[type] || icon_map['unknown'];",
  "    return <Icon color={color} style={{ width: size, height: size }} {...rest} />;",
  "  }",
  "}",
]);

web.Icon_string = web.Icon_lines.join('\n');

console.log('Web Icon Map created');

web.output_Icon_path = path.join(web.build_path, web.file_name);
try {
  fs.unlinkSync(web.output_Icon_path);
} catch(e) {
  console.log('no:', web.output_Icon_path);
}
console.log('into: '+ web.output_Icon_path );
fs.writeFileSync(web.output_Icon_path, web.Icon_string);





