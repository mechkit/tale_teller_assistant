export const display_types = [
  'card',
  'sample',
];

export const initial_state = {
  sidebar_open: false,
  show_cards: true,
  show_parent_cards: true,
  transition_duration: 123,
  transition_duration_slide: 333,
  display_mode: 'card', // or 'sample',
}

export default function reducer_UI( state= initial_state, action) {
  let new_state = {...state};
  if ( action.type === 'UI/sidebar_set_open' ) {
    new_state.sidebar_open = action.open;
  }
  if ( action.type === 'UI/parent_cards_toggle' ) {
    new_state.show_parent_cards = ! new_state.show_parent_cards
  }
  if ( action.type === 'UI/show_cards') {
    new_state.show_cards = true;
  }
  if ( action.type === 'UI/hide_cards') {
    new_state.show_cards = false;
  }
  if ( action.type === 'UI/switch-to-mode_sample') {
    new_state.display_mode = 'sample';
  }
  if ( action.type === 'UI/switch-to-mode_card') {
    new_state.display_mode = 'card';
  }
  return new_state
}
