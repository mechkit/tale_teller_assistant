import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import Deck from './components/Deck.jsx';
//import Sidebar from "react-sidebar";
import Sidebar from './components/Sidebar';

export default function App(props) {
  return (
    <div className="App" id='app'>
      <Sidebar />
      <Router>
        <div className='page' id='page'>
          <Route path='/'>
            <Deck/>
          </Route>
          <Route path='/docs'>
            <div className='docs'>
              documentation
            </div>
          </Route>
        </div>
      </Router>
    </div>
  );
}
