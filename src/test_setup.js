import React from 'react'
import reducer from './reducer';
import { Provider } from 'react-redux'
import { createStore } from 'redux';
//import { initial_state } from './reducer';
import { render } from '@testing-library/react'

export let card = {
  id: 42,
  type: 'character',
  sub_type: 'adventurer',
  data: {},
  deck: undefined,
  children: {},
  children_ids: [],
  connections: {},
  connection_ids: [],
  parents: {},
  parent_ids: [],
  icon_type: 'adventurer',
  description: 'test card',
  name: 'Test',
};

var state = {}

state.UI = {
  sidebar_open: false,
  show_cards: true,
  hide_parent_cards: false,
  transition_duration: 123,
  transition_duration_slide: 333,
}

state.world = {
  setting: 'fantasy',
  cards: {
    [card.id]: card
  },
  cards_ids: [card.id],
  world_card: card,
  active_card: card,
  active_card_id: null,
  filter: 'all',
  card_types: [],
  world_seed: '12345',
  hash_array: [],
}

let initial_state = state;

function render_with_store(
  ui,
  {
    initial_state = {},
    store = createStore(reducer, initial_state),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>
  }
  return render(ui, { wrapper: Wrapper, ...renderOptions })
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render_with_store }




/*
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')

beforeEach(() => {
  useSelectorMock.mockClear()
  useDispatchMock.mockClear()
})
*/

/*
test('renders learn react link', () => {
  render(<Deck />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
*/


/*
function renderWithProviders(ui,  reduxState ) {
  const store = createStore(reducer, reduxState);
  const div = document.createElement('div');
  return ReactDOM.render(<Provider store={store} >{ui}</Provider>, div);
}


it('renders without crashing', () => {
  //useSelectorMock.mockReturnValue(state)
  const div = document.createElement('div');
  //ReactDOM.render(<Deck card={card} />, div);
  renderWithProviders(<Deck />, state);
});
*/

/*
test('renders card content', () => {
  render(<Deck card={card}/>);
  const card_title = screen.getByText(/Test/i);
  expect(card_title).toBeInTheDocument();
  const card_description = screen.getByText(/test card/i);
  expect(card_description).toBeInTheDocument();
});
*/



