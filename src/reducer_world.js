import saveify from './functions/saveify';
import random_rpg_generator from 'random_rpg_generator';
import to_title from 'random_rpg_generator/dist/functions/to_title';

let world_seed_local = localStorage.getItem('world_seed');
let world_seed = world_seed_local || random_rpg_generator().seed_string();

var hash_string = window.location.hash.slice(2)
var hash_array = hash_string.split('/');
if ( hash_array[hash_array.length-1] === '' ) {
  hash_array.splice(-1);
}
hash_array[0] = hash_array[0] || world_seed;
hash_array[1] = hash_array[1] || 1;

var rrpgg = random_rpg_generator(world_seed);

function list_cards(card, cards) {
  cards = cards || {};
  if ( card && cards[card.id] === undefined ) {
    cards[card.id] = card;
    if( card.children_ids ){
      card.children_ids.forEach( card_id => {
        let sub_card = card.children[card_id];
        if ( cards[card_id] === undefined ) {
          cards = list_cards(sub_card, cards);
        }
      });
    }
    if( card.connection_ids ){
      card.connection_ids.forEach( card_id => {
        let sub_card = card.connections[card_id];
        if ( cards[card_id] === undefined ) {
          cards = list_cards(sub_card, cards);
        }
      });
    }
  }
  return cards;
}

function taleize_cards(cards) {
  Object.keys(cards).forEach( card_id => {
    let card = cards[card_id];
    card.side = card.side || 'up';
    card.selected = card.selected || false;
    card.new = card.new !== undefined ? card.new : true;
  });
  return cards;
}

function parse_hash_array(state, hash_array) {
  console.log('PARSE'); 
  // trim off empty input after 'abc/'
  if ( hash_array[hash_array.length-1] === '' ) {
    hash_array.splice(-1);
  }
  if ( ! hash_array || hash_array.length === 0 ) {
    state = set_state(state);
  }
  let new_world_seed = hash_array[0];
  if ( new_world_seed !== state.world_seed ) {
    state = set_state(state, new_world_seed);
  }
  let active_card_id = hash_array[1];
  if ( isNaN( Number( active_card_id ) ) ) { // is not number
    console.log(active_card_id);
    if ( state.named_cards[active_card_id] !== undefined ) { // should be a named card
      console.log(state.named_cards[active_card_id]);
      active_card_id = state.named_cards[active_card_id].id;
    } else { // not a real named card
      active_card_id = false;
    }
  }
  if ( ! isNaN( Number( active_card_id ) ) ) { // if is number
    if ( state.cards[active_card_id] === undefined ) { // and the number is not a card id
      active_card_id = false; 
    } // else active_card_id is fine as is
  }
  active_card_id = active_card_id || state.named_cards['world'].id;
  console.log(active_card_id);
  state.active_card = state.cards[active_card_id];
  console.log(state.cards[active_card_id]);
  console.log(state.active_card);
  return state;
}

function set_state(state, seed_string) {
  seed_string = seed_string || world_seed;
  if ( seed_string !== state.world_seed ) {
    state.world_seed = seed_string;
    rrpgg = random_rpg_generator(seed_string);
    state.named_cards.void = null;
  }
  // TODO: do not rebuild every url change
  //rrpgg.mk_ref_cards();
  // make the world
  if ( state.named_cards.void === null ) {
    state.cards = {};
    let void_card = rrpgg.card_new('void');
    state.named_cards.void = void_card;
    let world_card = rrpgg.card_new('world',{card_name:'The World'});
    void_card.add_child(world_card);
    state.named_cards.world = world_card;
    let reference_card = rrpgg.library.card;
    world_card.add_connection(reference_card);
    // add subcards to the world
    for ( var i=0; i<3; i++) {
      let civ_card = rrpgg.card_new('civilization');
      state.named_cards.world.add_child(civ_card);
    }

    let category_cards = rrpgg.card_new('category',{card:world_card});
    state.named_cards.category = category_cards;
    category_cards.add_parent_only(world_card);
    world_card.add_connection_oneway(category_cards);
    state.cards_by_type = category_cards.data.cards_by_type;
    state.card_types_in_world = Object.keys(state.cards_by_type);
    let samples_card = rrpgg.card_new('group', {name:'Samples of ...'})
    world_card.add_connection_oneway(samples_card);
    state.card_types_in_world.forEach( card_type => {
      console.log(card_type);
      let category_card = state.cards_by_type[card_type];
      let sample_card = rrpgg.card_new('sample', { 
        name: `Sample of ${card_type}`,
        card_to_sample: category_card,
      })
      samples_card.add_child(sample_card);
      /*
      */
    });


    /*
    let sample_card = rrpgg.card_new('sample',{card:world_card});
    state.named_cards.sample = sample_card;
    sample_card.add_parent_only(world_card);
    world_card.add_connection_oneway(sample_card);
    state.cards_to_shuffle.push(sample_card);
    state.cards_to_resample.push(sample_card);
    let link_card = rrpgg.card_new('custom',{
      name: 'Samples',
      icon_type: 'world',
      description: `Sample world cards`,
      data: {
        hash: `#/${state.world_seed}/sample`,
      }
    })
    world_card.add_connection_oneway(link_card);
    */
    /*
    state.card_type_links = state.card_types_in_world.map( card_type => {
      // TODO: make link cards for each category, to be displayed at /F32#2/sample
      // links to /F3Sd#@3/sample/character, etc.
      let [type, sub_type] = card_type.split('.');
      let name = to_title(type);
      if ( sub_type ) {
        name += ' (' + to_title(sub_type) + ')';
      }
      let link_card = rrpgg.card_new('custom',{
        name,
        icon_type: card_type,
        description: `Sample ${card_type} cards`,
        data: {
          hash: `#/${state.world_seed}/sample/${card_type}`,
        }
      })
      if ( state.named_cards[card_type] === undefined ) { // make sure not to overwrite
        state.named_cards[card_type] = link_card;
      }
      state.named_cards['world'].add_child(link_card);
      return link_card;
    })
    */
    state.named_cards_names = Object.keys(state.named_cards);
    state.cards = list_cards(void_card);
    taleize_cards(state.cards);
    state.card_ids = Object.keys(state.cards);
  }
  state = parse_hash_array(state,hash_array);
  return state;
};

export var initial_state = {
  setting: 'fantasy',
  cards: {},
  cards_ids: [],
  card_types: rrpgg.card_new(),
  card_types_in_world: [],
  card_type_links: null,
  cards_by_type: {},
  cards_to_shuffle: [],
  cards_to_resample: [],
  sample: {},
  sample_type: 'none',
  named_cards: {
    void: null,
    world: null,
    sample: null,
    content: null,
    category: null,
  },
  card_type_card: null,
  card_type_cards: null,
  active_card: null,
  // active_card_id: null,
  filter: 'all',
  world_seed,
  hash_array,
}
console.log(initial_state.card_types)
initial_state = set_state(initial_state, world_seed);
// initial_state.active_card = initial_state.named_cards.world;


/*
let saved_world_string = localStorage.getItem('world_string');
if ( saved_world_string !== 'undefined' ) {
  let saved = {};
  saved['deck'] = false;
  if ( saved_world_string ) {
    saved['deck'] = JSON.parse(saved_world_string);
  }
  // TODO: import saved
}
*/

export default function reducer_world(state = initial_state, action) {
  console.log('ACTION:', action)
  let new_state = {...state};
  switch (action.type) {
    case 'world/open_card': {
      window.scrollTo(0,0);
      let id = action.id;
      new_state.active_card = new_state.cards[id]
      break;
    }
    case 'world/new_card': {
      let type = action.card_type;
      let card = rrpgg.card_new(type);
      card.side = card.side || 'up';
      card.selected = card.selected || false;
      card.new = card.new !== undefined ? card.new : true;
      new_state.active_card = rrpgg.card( new_state.active_card, {type:'add_child', card} );
      new_state.cards[card.id] = card;
      break;
    }
    case 'world/set_filter': {
      new_state.filter = action.filter;
      break;
    }
    case 'world/sort': {
      new_state.active_card.deck = rrpgg.card(new_state.active_card.deck, {type:'sort'});
      break;
    }
    case 'world/new_url': {
      new_state = parse_hash_array(new_state, action.hash_array);
      window.scrollTo(0,0);
      break;
    }
    case 'world/samples': {
      new_state.sample_type = 'samples';
      new_state.sample.type = action.sample_type;
      new_state.sample.number = action.sample_number;
      break;
    }
    case 'world/sample': {
      new_state.sample_type = 'sample';
      new_state.sample.type = action.sample_type;
      new_state.sample.number = action.sample_number;
      break;
    }
    case 'world/shuffle': {
      new_state.cards_to_shuffle.forEach( card_to_shuffle => {
        rrpgg.shuffle_list( card_to_shuffle.children_ids );
        rrpgg.shuffle_list( card_to_shuffle.connection_ids );
      })
      break;
    }
    case 'world/resample': {
      new_state.cards.cards_to_resample.forEach( card_to_shuffle => {
        card_to_shuffle.update();
      })
      break;
    }
    default:
      break;
  }

  if ( new_state.active_card && new_state.active_card.children_ids ) {
    let card = new_state.active_card;
    if ( new_state.filter !== 'all' ){
      card.children_ids_show = []
      let filter = new_state.filter.split(' ');
      card.children_ids.forEach( id => {
        let card = new_state.cards[id];
        if ( filter.includes(card.type) ){
          card.children_ids_show.push(id);
        }
      });
    } else {
      card.children_ids_show = [...card.children_ids];
    }
  }
  if ( new_state.active_card && new_state.active_card.connection_ids ) {
    let card = new_state.active_card;
    if ( new_state.filter !== 'all' ){
      card.connection_ids_show = []
      let filter = new_state.filter.split(' ');
      card.connection_ids.forEach( id => {
        let card = new_state.cards[id];
        if ( filter.includes(card.type) ){
          card.connection_ids_show.push(id);
        }
      });
    } else {
      card.connection_ids_show = [...card.connection_ids];
    }
  }
  console.log(state.active_card.name);
  let card = new_state.active_card;
  card.backlink_ids_show = [...card.backlink_ids];

  /*
  new_state.save_string = '';
  let save_string = saveify(new_state);
  new_state.save_string = save_string;
  */

  if ( new_state.active_card === undefined ) {
    console.warn('OOPS, no active_card, going to the world...');
    //new_state.active_card = new_state.named_cards.world;
  }

  //console.log(new_state.sample.type)
  // console.log(new_state.sample_type);
  // console.log(new_state.sample);
  if ( new_state.sample.type !== undefined ) {
    new_state.sample.number = action.sample_number || 3;
    new_state.sample.cards = [];
    //console.log( new_state.card_types )
    if ( new_state.card_types.includes(new_state.sample.type) ) {
      let cards_of_type = new_state.cards_by_type[new_state.sample.type];
      let s = 1;
      while ( cards_of_type && new_state.sample.cards.length < new_state.sample.number && s<50 ){
        let random = rrpgg.pick_one( cards_of_type, true );
        if ( ! new_state.sample.cards.includes( random ) ) {
        //if ( ! new_state.sample.cards.map(card=>card.id).includes( random.id ) ) {
          new_state.sample.cards.push( random );
        }
        s++;
      };
    }
  } else if( new_state.sample_type === 'samples' ) {
    new_state.sample.cards = new_state.named_cards.sample.connection_ids.map( id => new_state.named_cards.sample.connections[id] );
  } else if( new_state.sample_type === 'sample' ) {
    new_state.sample.cards = new_state.card_type_links;
    new_state.sample.cards = rrpgg.shuffle_list( new_state.sample.cards, true );
  } else {
    new_state.sample.cards = [];
  }
  if ( new_state.sample.cards.length === 0 ) {
    new_state.sample.cards.push( new_state.named_cards.void );
  }
  var world_string = JSON.stringify( new_state.deck );
  localStorage.setItem('world_string', world_string);
  localStorage.setItem('world_seed', new_state.world_seed);

  return new_state;
}
