import random_rpg_generator from 'random_rpg_generator';
import { display_types } from '../reducer_UI';

export default function disphash(action) {
  let hash = window.location.hash;
  let hash_value_array = action.hash_value_array || false;
  if ( ! hash_value_array ) {
    hash_value_array = [];
    if ( hash !== '' && hash !== '#' && hash !== '#/' ){
      hash_value_array = hash.slice(2).split('/');
    }
  }
  // console.log(action, hash_value_array)
  let world_seed = hash_value_array[0];
  let active_card_id = hash_value_array[1];

  if ( action.type === 'world/open_card' ) {
    hash_value_array = [
      hash_value_array[0],
      action.id,
    ];
  }

  if ( action.type === 'hash/new_world_seed' ) {
    let rrpgg = random_rpg_generator();
    hash_value_array[0] = rrpgg.seed_string(true);
  }
  if ( action.type === 'hash/new_sample_type' ) {
    //hash_value_array[2] = action.sample_type || rrpgg.pick_one( Object.keys(new_state.cards_by_type) );
    //hash_value_array[3] = action.sample_number || 3;
  }

  if ( action.type === 'hash/pick_new_sample_type') {
    hash_value_array = [
      hash_value_array[0],
      'sample',
    ]
  }

  //hash_value_array[0] = world_seed;
  //hash_value_array[1] = active_card_id;

  window.location.hash = '#/' + hash_value_array.join('/');
}
