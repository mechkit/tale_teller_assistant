function is_shown(element) {
  let classes = element.className.split(' ');
  let index = classes.indexOf('show');
  return index > -1 ;
}

export default function show_hide_menu(target_id, show_class_name){
  // console.log('show_hide_menu')
  show_class_name = show_class_name || 'show';
  let element = document.getElementById(target_id);
  let classes = element.className.split(' ');
  let index = classes.indexOf(show_class_name);
  const outside_click_listener = event => {
    if (!element.contains(event.target) && is_shown(element)) { // or use: event.target.closest(selector) === null
      remove_click_listener();
      show_hide_menu(target_id);
    }
  }
  const remove_click_listener = () => {
    document.removeEventListener('click', outside_click_listener)
  }
  if ( index === -1 ) {
    is_shown(element)
    document.addEventListener('click', outside_click_listener)
    setTimeout( ()=> {
      classes.push(show_class_name);
      element.className = classes.join(' ');
    },0);
  } else {
    classes.splice(index,1);
    element.className = classes.join(' ');
  }
};
