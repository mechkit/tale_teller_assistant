export default function class_on_off(target_class_name, on_off_class_name){
  on_off_class_name = on_off_class_name || 'on';
  let elements = [...document.getElementsByClassName(target_class_name)];
  elements.forEach( element => {
    let classes = element.className.split(' ');
    let index = classes.indexOf(on_off_class_name);
    if ( index > -1 ) {
      classes.splice(index,1);
    } else {
      classes.push(on_off_class_name);
    }
    element.className = classes.join(' ');
  });
};
