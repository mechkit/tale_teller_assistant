import { ReactComponent as AxeSword } from './axe-sword.svg';
import { ReactComponent as Chest } from './chest.svg';
import { ReactComponent as World } from './world.svg';

const icon_map = {
  'axe-sword': AxeSword,
  'chest': Chest,
  'world': World,
};

export default icon_map;