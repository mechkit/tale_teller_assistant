import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import reducer from './reducer'
import { initial_state } from './reducer'
import App from './App.jsx';
import reportWebVitals from './reportWebVitals';
//import history from 'history/browser';
import Hash_router from 'hash_router';
import disphash from './functions/disphash';
import 'fontsource-roboto';
import { display_types } from './reducer_UI';

const store = createStore(
  reducer,
  initial_state,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

/*
history.listen(({ location, action }) => {
  console.log(action, location.pathname, location.state);
  store.dispatch({type:'deck/change_location',location:location});
});

*/

var named_cards_names = initial_state.world.named_cards_names;
store.subscribe(()=>{
  let state = store.getState();
  named_cards_names = state.world.named_cards_names;
})

let hash_value_array_last = [];
const hash_router = Hash_router( (hash_value_array) => {
  console.log('#', hash_value_array);
  if ( hash_value_array === false ) {
    disphash({type:'hash/new_world_seed'});
    return;
  }
  if ( named_cards_names.includes(hash_value_array[0]) ) { // probably outside url request for sample cards
    console.log('OUTSIDE');
    hash_value_array.unshift(undefined);
    disphash({type:'hash/new_world_seed',hash_value_array});
    return;
  } else if (named_cards_names.includes(hash_value_array[1]) ) { // has a world seed, and a request for sample
    store.dispatch({type:'UI/switch-to-mode_sample'});
    let sample_type;
    let sample_number;
    if ( hash_value_array[2] !== undefined && isNaN( hash_value_array[2] ) ) {
      sample_type = hash_value_array[2];
    }
    if ( hash_value_array[3] !== undefined && ! isNaN( hash_value_array[3] ) ) {
      sample_number = hash_value_array[3];
    }
    if ( hash_value_array[1] === 'sample' ) {
      store.dispatch({type:'world/sample',sample_type,sample_number});
    } else if ( hash_value_array[1] === 'samples' ) {
      store.dispatch({type:'world/samples',sample_type,sample_number});
    }
    hash_value_array_last = hash_value_array;
  } else if ( hash_value_array[0].length !== 8 || ! /[A-Z]/.test(hash_value_array[0] ) || ! /[0-9]/.test(hash_value_array[0] )) {
    console.log('START NEW', hash_value_array);
    hash_value_array[0] = undefined;
    disphash({type:'hash/new_world_seed',hash_value_array});
    return;
  } else { // ready to display a card
    if ( hash_value_array[0] !== hash_value_array_last[0] || hash_value_array[1] !== hash_value_array_last[1] ) {
      store.dispatch({type:'UI/switch-to-mode_card'});
      store.dispatch({type:'UI/hide_cards'});
      setTimeout( ()=> {
        store.dispatch({type:'world/new_url',hash_array:hash_value_array});
        store.dispatch({type:'UI/show_cards'});
      },123);
    }
    hash_value_array_last = hash_value_array;
  }
});
hash_router();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>,
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
