import React from 'react'
import { render_with_store, screen } from './test_setup'
import App from './App';

it('renders without crashing', () => {
  render_with_store(<App />)
});

it('renders with text', () => {
  render_with_store(<App />)
  expect(screen.getByText(/Tale Tellers Assistant/i)).toBeInTheDocument()
})



