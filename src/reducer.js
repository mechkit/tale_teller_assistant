import { combineReducers } from 'redux'
import world from './reducer_world';
import { initial_state as initial_state_world } from './reducer_world';
import UI from './reducer_UI';
import { initial_state as initial_state_UI } from './reducer_UI';

export const initial_state = {
  world: initial_state_world,
  UI: initial_state_UI,
};

const reducer = combineReducers({
  world,
  UI,
});

export default reducer
