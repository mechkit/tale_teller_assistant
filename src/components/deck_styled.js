import styled from 'styled-components';

export const TableDivider = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 450px;
  margin-bottom: 4em;
`;
export const TableDividerHome = styled(TableDivider)`
  opacity: 1.0;
`;
export const TableDividerParent = styled(TableDivider)`
  opacity: 1.3;
`;
export const TableDividerChildren = styled(TableDivider)`
  opacity: 1.3;
`;
export const CardSpacer = styled(TableDivider)`
  width: 1em;
`;
export const SidebarMenu = styled.div`
  position: absolute;
  left: 0px;
  top: 2px;
  padding: 0px;
  margin: 0px;
  fill: grey;
  z-index: 10;
`;
export const SideButtons = styled.div`
  position: absolute;
  left: 5px;
  top: 42px;
  padding: 0px;
  margin: 0px;
  z-index: 10;
`;
export const SideButton = styled.div`
  padding-top: 23px;
`;

export const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`

export const CardPlace = styled.div`
  margin: 12px;
  margin-bottom: 4em;
  width: 350px;
  height: calc( 350px / 350 * 450  );
  border-radius: 10px;
  background-color: transparent;
`

export const CardPlaceOutline = styled(CardPlace)`
  border: 3px dotted grey;
`

export const FadeOut = styled.div`
  display: inline;
  transition: 123ms;
  opacity: ${ stage => stage==='exiting'? 0 : 1 } ;
  display: ${ stage => stage==='exited'? 'none': 'inline'};
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`