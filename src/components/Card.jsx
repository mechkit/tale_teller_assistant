import React from 'react';
import './card.css';
import Icon from './Icon.jsx';
import { icon_names } from './Icon.jsx';
import { useSelector, useDispatch } from 'react-redux'
import { usehandle_action } from 'react-redux'
import ContentEditable from 'react-contenteditable'
import { CSSTransition } from 'react-transition-group';
import disphash from '../functions/disphash.js';
import styled from 'styled-components';

const CardDescriptionSection = styled.div`
  display: flex;
  align-content: flex-start;
  align-items: center;
  overflow: scroll;
`;
const CardDescription = styled.div`
  text-align: left;
  height: 100%;
  width: 100%;
`;
const CardLink = styled.span`
{
  position:absolute;
  width:100%;
  height:100%;
  top:0;
  left: 0;
  z-index: 111;
}
`;
const SpanButtonLink = styled.span`
  cursor: pointer;
  position:relative;
  text-decoration: underline;
  text-decoration-color: lightblue;
  padding-right: 4px;
  padding-left: 4px;
  border: 1px lightblue solid;
  /* border-bottom: 1px blue solid; */
  border-radius: 4px;
  &:hover {
    border: 1px blue solid;
    text-decoration-color: blue;
  }
  z-index: 112;
`;
const SpecialSpanButtonLink = styled(SpanButtonLink)`
  text-align: center;
  color: red;
  margin-top: 1em;
  border: 1px lightblue solid;
  &:hover {
    border: 1px red solid;
    text-decoration-color: red;
    svg g path {
      fill: red;
    }
  }
`

function click(e,card){
  e.preventDefault();
  if ( card.data.href !== undefined ) {
    window.open(card.data.href);
  } else if ( card.data.hash !== undefined ) {
    window.location.hash = card.data.hash;
  } else if (card.openable) {
    disphash({type:'world/open_card',id:card.id});
  }
}
 
const regex_link = /(\[\[[^\]]*\]\])/gm;

var Card = function(props) {
  const card = props.card;
  const icon = card.icon;
  let icon_type = card.icon_type;
  if ( ! icon_names.includes(icon_type) ) {
    icon_type = icon_type.split('.')[0];
  }
  const selected = card.selected;
  const card_new = card.new;
  const card_description_lines = card.description_lines;
  /*
  if ( card_description.length > 542 ) {
    card_description = card_description.slice(0,542) + '...';
  }
  */
  let card_description = [];
  card.description_lines.forEach( description_line => {
    let description_line_parts = description_line.split(regex_link);
    if ( description_line_parts.length > 0 ) {
      description_line_parts.forEach( description_part => {
        if ( description_part.slice(0,2) === '[[' && description_part.slice(-2) === ']]' ) {
          let linked_card_id = description_part.slice(2,-2);
          let card_name = Object.assign( {}, card.children, card.connections, card.links )[linked_card_id].title;
          description_part = <SpanButtonLink onClick={(e)=>{disphash({type:'world/open_card',id:linked_card_id}); e.preventDefault();}}>{card_name}</SpanButtonLink>;
        }
        card_description.push(description_part);
      });
    } else {
      card_description = card_description.concat(description_line_parts);
    }
    card_description.push( <br /> );
    return description_line;
  });

  const dispatch = useDispatch();
  const has_children = !! card.children_ids.length;
  let className = selected ? 'card card_selected' : 'card';
  className = props.location === 'parents' ? className + ' card_parent' : className;
  className = props.location === 'active' ? className + ' card_focus' : className;
  className = props.location === 'connection' ? className + ' card_connection' : className;
  className = props.location === 'backlink' ? className + ' card_backlink' : className;
  card.openable = props.location !== 'active' && card.type !== 'void';
  return (
    <div className={className}>
      <CardLink onClick={(e)=>{ click(e,card); }}></CardLink>
      <div className='CARD_ID'>{card.id}</div>
      <div className='card_title_section'>
        <Icon type={icon_type} icon={icon} size='3em' />
        {/*<ContentEditable className='card_name' html={card.title} onChange={(e)=> dispatch({type:'world/new_value',id, 'prop':'name',value:e.target.value}) } tagName='span' />*/}
        <span className='card_name'>{card.title}</span>
      </div>
      {/*
      <div className='card_stats'>
        {card.stats_list.map(stat_name=>{
          let stat = card.stats[stat_name];
          return <div key={stat_name} title={stat.description}>{stat_name}: {stat.value}</div>;
        })}
      </div>
      */}
      <div className='card_divider_section'>
        <div className='card_divider_line'></div>
          <div className='card_open_frame' >
            <Icon type='TTA' />
          </div>
      </div>
      <CardDescriptionSection>
        {/*<ContentEditable className='card_description' html={card_description} onChange={(e)=> dispatch({type:'world/new_value',id, 'prop':'description',value:e.target.value}) } tagName='div' />*/}
        <CardDescription>{card_description}</CardDescription>
      </CardDescriptionSection>
      {/*
      { card_new &&
        <div className='card_icon_section'>
          <div className='card_icon_frame' data-tip='save card' onClick={(e)=>{ dispatch({type:'world/save_card',id}); e.preventDefault();}} >
            <Icon type='card_save' />
          </div>
          <div className='card_icon_frame' data-tip='shuffle card' onClick={(e)=>{ dispatch({type:'world/shuffle_card',id}); e.preventDefault();} } >
            <Icon type='card_reload' />
          </div>
        </div>
      }
      { ! card_new &&
        <div className='card_icon_section'>
          <div className='card_icon_frame' data-tip='save card' onClick={(e)=>{ dispatch({type:'world/card_click',id}); e.preventDefault();}} >
            <Icon type='menu' />
          </div>
        </div>
      }
      { ! card_new && selected &&
        <div className='card_icon_section'>
          <div className='card_icon_frame' data-tip='save card' onClick={(e)=>{ dispatch({type:'world/discard',id}); e.preventDefault();}} >
            <Icon type='discard' />
          </div>
          <div className='card_icon_frame' data-tip='save card' onClick={(e)=>{ dispatch({type:'world/card_click',id}); e.preventDefault();}} >
            <Icon type='menu' />
          </div>
        </div>
      }
      */}
      { props.is_root &&
        <SpecialSpanButtonLink onClick={()=>disphash({type:'world/new_world_seed'})} title='Randomize world'>
          <Icon type='dynamite'/>
          <Icon type='world_destroy'/>
          <Icon type='shatter'/>
          <Icon type='randomize'/>
          <Icon type='world'/>
        </SpecialSpanButtonLink>
      }
    </div>
  );
}




export default Card;
