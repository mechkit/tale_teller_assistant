import React from 'react';
import random_rpg_generator from 'random_rpg_generator';
import './random.css';

const rrpgg = random_rpg_generator();

class Random extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      value:'_'
    };
    this.new_random = this.new_random.bind(this);
  }

  new_random() {
    this.setState( (state,props) => {
      return {
        value: rrpgg[state.type](),
      }
    });
  }

  componentDidMount() {
    this.new_random();
  }

  render() {
    const state = this.state
    var value = state.value;
    if ( value.constructor === Object ){
      value = JSON.stringify(value,null,2);
    }
    return <li className='random' onClick={this.new_random} key={this.props.id}>
      <span className='random_name'>{state.type}: </span>
      <span className='random_value'>{value}</span>
    </li>;
  }
}


export default Random;
