import React from 'react'
import { render_with_store, card, screen } from '../test_setup'
import Card from './Card';

it('renders without crashing', () => {
  render_with_store(<Card card={card} display={true} location='active' />, {});
});

it('renders card content', () => {
  render_with_store(<Card card={card} display={true} location='active' />, {});
  const card_title = screen.getByText(/Test/i);
  expect(card_title).toBeInTheDocument();
  const card_description = screen.getByText(/test card/i);
  expect(card_description).toBeInTheDocument();
});

