import React from 'react';
//import random_rpg_generator from 'random_rpg_generator';
import Random from './Random.jsx';

//const rrpgg = random_rpg_generator();

class RandomList extends React.Component {
  constructor(props){
    super(props);
    this.state = {...props,
      randoms: [
        'age',
        'odds',
        'quirk',
        'name',
        'character',
      ],
    };
  }

  render() {
    const type = this.state.type;
    // console.log(type)
    return (
      <div>
        <ul>
          {this.state.randoms.map( (type,i) => {
            return <Random type={type} id={i} />;
          })}
        </ul>
      </div>
    )
  }
}


export default RandomList;
