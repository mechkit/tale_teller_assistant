import styled from 'styled-components';

import { ReactComponent as TtaIcon } from '../icons/TTA-icon.svg';
import { ReactComponent as AllSeeingEye } from '../icons/all-seeing-eye.svg';
import { ReactComponent as AnimalHide } from '../icons/animal-hide.svg';
import { ReactComponent as AnvilImpact } from '../icons/anvil-impact.svg';
import { ReactComponent as ArrowLeftRight } from '../icons/arrow-left-right.svg';
import { ReactComponent as ArrowsExchange } from '../icons/arrows-exchange.svg';
import { ReactComponent as AxeSword } from '../icons/axe-sword.svg';
import { ReactComponent as Backpack } from '../icons/backpack.svg';
import { ReactComponent as Barn } from '../icons/barn.svg';
import { ReactComponent as Bed } from '../icons/bed.svg';
import { ReactComponent as Beehive } from '../icons/beehive.svg';
import { ReactComponent as BlockHouse } from '../icons/block-house.svg';
import { ReactComponent as BookCover } from '../icons/book-cover.svg';
import { ReactComponent as Bookmarklet } from '../icons/bookmarklet.svg';
import { ReactComponent as Bookshelf } from '../icons/bookshelf.svg';
import { ReactComponent as Boots } from '../icons/boots.svg';
import { ReactComponent as BowArrow } from '../icons/bow-arrow.svg';
import { ReactComponent as BoxArrowInRight } from '../icons/box-arrow-in-right.svg';
import { ReactComponent as BoxArrowRight } from '../icons/box-arrow-right.svg';
import { ReactComponent as BxBuildingHouse } from '../icons/bx-building-house.svg';
import { ReactComponent as BxFilterAlt } from '../icons/bx-filter-alt.svg';
import { ReactComponent as BxMenu } from '../icons/bx-menu.svg';
import { ReactComponent as BxTrash } from '../icons/bx-trash.svg';
import { ReactComponent as CampCookingPot } from '../icons/camp-cooking-pot.svg';
import { ReactComponent as CandleHolder } from '../icons/candle-holder.svg';
import { ReactComponent as Capitol } from '../icons/capitol.svg';
import { ReactComponent as CardChecklist } from '../icons/card-checklist.svg';
import { ReactComponent as CardPickup } from '../icons/card-pickup.svg';
import { ReactComponent as CardQueenHearts } from '../icons/card-queen-hearts.svg';
import { ReactComponent as CardRandom } from '../icons/card-random.svg';
import { ReactComponent as CellarBarrels } from '../icons/cellar-barrels.svg';
import { ReactComponent as Chest } from '../icons/chest.svg';
import { ReactComponent as Cow } from '../icons/cow.svg';
import { ReactComponent as CryptEntrance } from '../icons/crypt-entrance.svg';
import { ReactComponent as DiceSixFacesFive } from '../icons/dice-six-faces-five.svg';
import { ReactComponent as DoughRoller } from '../icons/dough-roller.svg';
import { ReactComponent as DungeonGate } from '../icons/dungeon-gate.svg';
import { ReactComponent as Dynamite } from '../icons/dynamite.svg';
import { ReactComponent as ElvenCastle } from '../icons/elven-castle.svg';
import { ReactComponent as EmptyHourglass } from '../icons/empty-hourglass.svg';
import { ReactComponent as ExplodingPlanet } from '../icons/exploding-planet.svg';
import { ReactComponent as FamilyHouse } from '../icons/family-house.svg';
import { ReactComponent as Fireplace } from '../icons/fireplace.svg';
import { ReactComponent as Flour } from '../icons/flour.svg';
import { ReactComponent as GridXGapFill } from '../icons/grid-3x3-gap-fill.svg';
import { ReactComponent as Grid } from '../icons/grid.svg';
import { ReactComponent as HarpoonChain } from '../icons/harpoon-chain.svg';
import { ReactComponent as HelpCircle } from '../icons/help-circle.svg';
import { ReactComponent as HillFort } from '../icons/hill-fort.svg';
import { ReactComponent as HorseHead } from '../icons/horse-head.svg';
import { ReactComponent as HutsVillage } from '../icons/huts-village.svg';
import { ReactComponent as LeatherArmor } from '../icons/leather-armor.svg';
import { ReactComponent as LibraryBooks } from '../icons/library-books.svg';
import { ReactComponent as LocationCity } from '../icons/location_city.svg';
import { ReactComponent as Log } from '../icons/log.svg';
import { ReactComponent as MakeGroup } from '../icons/make-group.svg';
import { ReactComponent as Meat } from '../icons/meat.svg';
import { ReactComponent as MenuFold } from '../icons/menu-fold.svg';
import { ReactComponent as MenuUnfold } from '../icons/menu-unfold.svg';
import { ReactComponent as Menu } from '../icons/menu.svg';
import { ReactComponent as Miner } from '../icons/miner.svg';
import { ReactComponent as Necklace } from '../icons/necklace.svg';
import { ReactComponent as NewFile } from '../icons/new-file.svg';
import { ReactComponent as Note } from '../icons/note.svg';
import { ReactComponent as OfficeBuilding } from '../icons/office-building.svg';
import { ReactComponent as OpenFolder } from '../icons/open-folder.svg';
import { ReactComponent as Papers } from '../icons/papers.svg';
import { ReactComponent as PersonFill } from '../icons/person-fill.svg';
import { ReactComponent as PictureInPictureExitFill } from '../icons/picture-in-picture-exit-fill.svg';
import { ReactComponent as PieChartLine } from '../icons/pie-chart-2-line.svg';
import { ReactComponent as Place } from '../icons/place.svg';
import { ReactComponent as PlantRoots } from '../icons/plant-roots.svg';
import { ReactComponent as PotionBall } from '../icons/potion-ball.svg';
import { ReactComponent as PowderBag } from '../icons/powder-bag.svg';
import { ReactComponent as QuestionOctagonFill } from '../icons/question-octagon-fill.svg';
import { ReactComponent as Reload } from '../icons/reload.svg';
import { ReactComponent as RolledCloth } from '../icons/rolled-cloth.svg';
import { ReactComponent as SewingString } from '../icons/sewing-string.svg';
import { ReactComponent as Shatter } from '../icons/shatter.svg';
import { ReactComponent as Shirt } from '../icons/shirt.svg';
import { ReactComponent as Stable } from '../icons/stable.svg';
import { ReactComponent as StoneWheel } from '../icons/stone-wheel.svg';
import { ReactComponent as Storefront } from '../icons/storefront.svg';
import { ReactComponent as SwapBag } from '../icons/swap-bag.svg';
import { ReactComponent as Toolbox } from '../icons/toolbox.svg';
import { ReactComponent as Tree } from '../icons/tree.svg';
import { ReactComponent as TreeChildren } from '../icons/tree_children.svg';
import { ReactComponent as TreeParent } from '../icons/tree_parent.svg';
import { ReactComponent as TrumpetFlag } from '../icons/trumpet-flag.svg';
import { ReactComponent as Village } from '../icons/village.svg';
import { ReactComponent as VineLeaf } from '../icons/vine-leaf.svg';
import { ReactComponent as Windmill } from '../icons/windmill.svg';
import { ReactComponent as WineBottle } from '../icons/wine-bottle.svg';
import { ReactComponent as WoodBeam } from '../icons/wood-beam.svg';
import { ReactComponent as World } from '../icons/world.svg';
import { ReactComponent as WorldReentry } from '../icons/world_reentry.svg';

const icon_map = {
  'abandoned': CryptEntrance,
  'all': AllSeeingEye,
  'arrow_left_arrow': ArrowLeftRight,
  'backlink': HarpoonChain,
  'box_arrow_right': BoxArrowRight,
  'building': BxBuildingHouse,
  'building.animal_yard': Cow,
  'building.appiary': Beehive,
  'building.baker': DoughRoller,
  'building.blacksmith': AnvilImpact,
  'building.bookbinder': BookCover,
  'building.butcher': Meat,
  'building.carpenter': WoodBeam,
  'building.chandlers': CandleHolder,
  'building.farm': PlantRoots,
  'building.furrier': AnimalHide,
  'building.inn': Bed,
  'building.jeweler': Necklace,
  'building.leather tanner': AnimalHide,
  'building.mill': StoneWheel,
  'building.mine': Miner,
  'building.paper_maker': Papers,
  'building.refinery': Fireplace,
  'building.sawmill': WoodBeam,
  'building.shoemaker': Boots,
  'building.smithy': AnvilImpact,
  'building.spice_market': PowderBag,
  'building.spice_mixer': PowderBag,
  'building.stable': Stable,
  'building.store': Storefront,
  'building.store-armor': LeatherArmor,
  'building.store-book': Bookshelf,
  'building.store-farm': Barn,
  'building.store-food': CampCookingPot,
  'building.store-general': Storefront,
  'building.store-item': Chest,
  'building.store-potion': PotionBall,
  'building.store-shoe': Boots,
  'building.store-weapon': AxeSword,
  'building.string_maker': SewingString,
  'building.tailor': Shirt,
  'building.town_hall': OfficeBuilding,
  'building.vineyard': VineLeaf,
  'building.weaver': RolledCloth,
  'building.wine_cellar': CellarBarrels,
  'building.winery': WineBottle,
  'building.wood yard': Log,
  'card_close': PictureInPictureExitFill,
  'card_open': DungeonGate,
  'card_reload': Reload,
  'card_save': BoxArrowInRight,
  'card_tree': Tree,
  'card_tree_parent': TreeParent,
  'card_tree_children': TreeChildren,
  'category': OpenFolder,
  'character': PersonFill,
  'character.adventurer': Backpack,
  'character.townfolk': PersonFill,
  'city': FamilyHouse,
  'city.hamlet': HutsVillage,
  'city.metropolis': Capitol,
  'city.town': BlockHouse,
  'city.village': Village,
  'civilization': TrumpetFlag,
  'connection': ArrowsExchange,
  'deck': LibraryBooks,
  'discard': BxTrash,
  'filter': BxFilterAlt,
  'flour mill': Flour,
  'fort': HillFort,
  'general_store': Storefront,
  'generic': NewFile,
  'group': Grid,
  'group.district': PieChartLine,
  'help': HelpCircle,
  'loot.armor': LeatherArmor,
  'loot.item': Chest,
  'loot.melee': AxeSword,
  'loot.ranged': BowArrow,
  'menu': Menu,
  'new_card': NewFile,
  'of': Grid,
  'place': Place,
  'playing_cards': CardQueenHearts,
  'quirk': SwapBag,
  'randomize': DiceSixFacesFive,
  'reference': Bookmarklet,
  'relationship': MakeGroup,
  'sample': GridXGapFill,
  'sample_pick': CardPickup,
  'sidebar_close': MenuFold,
  'sidebar_open': MenuUnfold,
  'task': CardChecklist,
  'tools': Toolbox,
  'unknown': QuestionOctagonFill,
  'void': EmptyHourglass,
  'world': World,
  'world_goto': WorldReentry,
  'world_destroy': ExplodingPlanet,
  'TTA': TtaIcon,
  'bx-menu': BxMenu,
  'card-random': CardRandom,
  'dynamite': Dynamite,
  'elven-castle': ElvenCastle,
  'horse-head': HorseHead,
  'location_city': LocationCity,
  'note': Note,
  'shatter': Shatter,
  'windmill': Windmill,
};

export const icon_names = Object.keys(icon_map);

const IconStr = styled.span`
  color: ${props=>props.color};
  font-size: ${props=>props.size};
`

export default function Icon( { type, size, color, icon, ...rest } ) {
  size = size || '1em';
  color = color || 'black';
  if ( icon ) {
    return <IconStr color={color} size={size} >{icon}</IconStr>
  } else {
    const Icon = icon_map[type] || icon_map['unknown'];
    return <Icon color={color} style={{ width: size, height: size }} {...rest} />;
  }
}