import {CopyToClipboard} from 'react-copy-to-clipboard';
import { useDispatch } from 'react-redux'
import './save.css';

function Save(props) {
  const dispatch = useDispatch()
  return (
    <div className="save">
      {/* <button className="close" onClick={props.close}>close</button> */}
      <h2>Save Deck</h2>
      <p>Save string:</p>
      <div>
        <textarea className='save_string' value={props.save_string} readOnly></textarea>
      </div>
      <CopyToClipboard text={props.save_string}>
        <button>Copy save string to clipboard</button>
      </CopyToClipboard>
      <div>
        <textarea className='save_string'></textarea>
      </div>
      {/*<button disabled onClick={dispatch({type:'world/load_save_string'})}>load deck</button>*/}
    </div>
  );
}

export default Save;
