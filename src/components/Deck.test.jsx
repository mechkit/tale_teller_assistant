import React from 'react'
import { render_with_store, screen } from '../test_setup'
import Deck from './Deck';

it('renders without crashing', () => {
  render_with_store(<Deck />, { initialState: { user: 'Redux User' } })
});
