import React from 'react';
import './Sidebar.css';
import { useSelector, useDispatch } from 'react-redux'
import Icon from './Icon.jsx';
import Save from './Save.jsx';
import About from './About.jsx';
import { CSSTransition } from 'react-transition-group';
import styled from 'styled-components';

const SidebarMenu = styled.div`
  position: absolute;
  left: 0px;
  top: 2px;
  padding: 0px;
  margin: 0px;
  z-index: 10;
`;

export default function Sidebar(props) {
  const state = useSelector(state => state);
  const dispatch = useDispatch()
  return (
    <div>
      <CSSTransition in={state.UI.sidebar_open} timeout={state.UI.transition_duration_slide} classNames='sidebar_cover' >
        <div className='sidebar_cover' onClick={()=> dispatch({type:'UI/sidebar_set_open',open:false})} >
        </div>
      </CSSTransition>
      <CSSTransition in={state.UI.sidebar_open} timeout={state.UI.transition_duration_slide} classNames='sidebar' >
        <div className='sidebar'>
          <SidebarMenu onClick={()=> dispatch({type:'UI/sidebar_set_open',open:false}) }>
            <Icon size='1.5em' type='sidebar_close'/>
          </SidebarMenu>
          <About />
          <hr/>
          <Save save_string={state.world.save_string} />
          <hr/>
          {/*<button onClick={()=>dispatch({type:'UI/help'})}><Icon type='help'/></button>*/}
        </div>
      </CSSTransition>
    </div>
  );
};
