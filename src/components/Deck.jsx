import './deck.css';
import React from 'react';
import {
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useRouteMatch,
  useHistory
} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import disphash from '../functions/disphash.js';
import Icon from './Icon.jsx';
import Card from './Card.jsx';
import { CSSTransition } from 'react-transition-group';
import { Transition } from 'react-transition-group';
import show_hide_menu from '../functions/show_hide_menu.js';
import {
  TableDivider,
  TableDividerHome,
  TableDividerParent,
  TableDividerChildren,
  CardSpacer,
  SidebarMenu,
  SideButtons,
  SideButton,
  FadeOut,
  Cards,
  CardPlace,
  CardPlaceOutline,
} from './deck_styled';


var Deck = function(props) {
  const UI = useSelector(state => state.UI);
  const world = useSelector(state => state.world);
  const dispatch = useDispatch();
  const cards = world.cards;
  const active_card = world.active_card;
  // console.log('active_card',active_card)
  const card_types = world.card_types;
  const filter = world.filter;

  const is_root = world.active_card.id === world.named_cards.world.id;
  const has_children = !! world.active_card.children_ids.length;

  if ( UI.display_mode === 'card' ) {
    document.title = `TTA: ${active_card.title}`;
  } else if ( UI.display_mode === 'sample' ) {
    document.title = `TTA: sample of ${world.sample.type}`;
  } else {
    document.title = `TTA`;
  }
  var parent_ids = active_card.parent_ids;
  if ( parent_ids.length === 0 ) parent_ids = [ world.named_cards.void.id ];
  const parent_Cards = parent_ids.map( id => {
    let parent_card = cards[id];
    return (
      <CardPlace key={parent_card.id} data-label='card'>
        <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames={'card_right'} >
          <Card card={parent_card} display={UI.show_parent_Cards} location='parents' />
        </CSSTransition>
      </CardPlace>
    );
  });
  const active_Card = (
    <CardPlaceOutline key={active_card.id} data-label='card'>
      <CSSTransition in={true} timeout={UI.transition_duration} classNames={`card_active_${UI.parent_card_direction}`} enter={true} exit={true}>
        <Card card={active_card} id={active_card.id} location='active' is_root={is_root} />
      </CSSTransition>
    </CardPlaceOutline>
  );
  const child_Cards = active_card.children_ids_show.map( id => {
    let sub_card = cards[id];
    // let has_children = !! sub_card.children_ids.length;
    return (
      <CardPlace key={sub_card.id} data-label='card'>
        <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames={'card_left'} >
          <Card card={sub_card} location='children'/>
        </CSSTransition>
      </CardPlace>
    );
  });
  const connection_Cards = active_card.connection_ids_show.map( id => {
    let sub_card = cards[id];
    // let has_connection = !! sub_card.connection_ids.length;
    return (
      <CardPlace key={sub_card.id} data-label='card'>
        <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames={'card_left'} >
          <Card card={sub_card} location='connection'/>
        </CSSTransition>
      </CardPlace>
    );
  });
  const backlink_Cards = active_card.backlink_ids_show.map( id => {
    let sub_card = cards[id];
    // let has_backlink = !! sub_card.backlink_ids.length;
    return (
      <CardPlace key={sub_card.id} data-label='card'>
        <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames={'card_left'} >
          <Card card={sub_card} location='backlink' />
        </CSSTransition>
      </CardPlace>
    );
  });
  const sample_Cards = world.sample.cards.map( sub_card => {
    // let has_children = !! sub_card.children_ids.length;
    return (
      <div className='card_place' key={sub_card.id} data-label='card'>
        <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames={'card_left'} >
          <Card card={sub_card} location='children'/>
        </CSSTransition>
      </div>
    );
  });
  return (
    <div className='deck'>
      <SidebarMenu onClick={()=> dispatch({type:'UI/sidebar_set_open',open:true}) }>
        <Icon size='1.5em' type='sidebar_open'/>
      </SidebarMenu>
      <SideButtons>
        <SideButton onClick={()=> dispatch({type:'UI/parent_Cards_toggle'})} >
          <Icon type='world_goto' size='1.5em' onClick={()=>disphash({type:'world/open_card',id:''})} />
        </SideButton>
        <SideButton onClick={()=> disphash({type:'hash/pick_new_sample_type'}) }>
          <Icon size='1.5em' type='sample_pick'/>
        </SideButton>
        <SideButton onClick={()=> dispatch({type:'UI/parent_cards_toggle'}) }>
          <Icon size='1.5em' type='card_tree_parent'/>
        </SideButton>
        <SideButton onClick={()=> dispatch({type:'world/sample_reload'}) }>
          <Icon size='1.5em' type='card_reload'/>
        </SideButton>
      </SideButtons>
      <Cards data-label='CARDS'>
        <CardSpacer key='a'></CardSpacer>
        <CardSpacer key='b'></CardSpacer>
        <CardSpacer key='c'></CardSpacer>
        { /*! UI.show_parent_cards && parent_Cards*/ }
        <Transition in={UI.show_parent_cards} timeout={UI.transition_duration}>
          { stage => {
            console.log(stage);
            return <FadeOut stage={stage}>
              {parent_Cards}
              { !! parent_Cards.length &&
                <TableDividerParent onClick={()=> dispatch({type:'UI/parent_cards_toggle'})} >
                  <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames='card_marker' >
                    <Icon type='card_tree_parent' size='2em' />
                  </CSSTransition>
                  <CardSpacer></CardSpacer>
                </TableDividerParent>
              }
            </FadeOut>
          }}
        </Transition>
        {active_Card}
        { !! child_Cards.length &&
          <TableDividerChildren>
            <CardSpacer></CardSpacer>
            <CSSTransition in={UI.show_cards} timeout={UI.transition_duration} classNames='card_marker' >
              <Icon type='card_tree_children' size='2em' />
            </CSSTransition>
          </TableDividerChildren>
        }
        {child_Cards}
        { !! connection_Cards.length &&
          <TableDividerChildren>
            <CardSpacer></CardSpacer>
            <CSSTransition in={UI.show_cards} timeout={UI.transition_duration*3} classNames='card_marker' >
              <Icon type='connection' size='2em' />
            </CSSTransition>
          </TableDividerChildren>
        }
        {connection_Cards}
        { !! backlink_Cards.length &&
          <TableDividerChildren>
            <CardSpacer></CardSpacer>
            <CSSTransition in={UI.show_cards} timeout={UI.transition_duration*3} classNames='card_marker' >
              <Icon type='backlink' size='2em' />
            </CSSTransition>
          </TableDividerChildren>
        }
        {backlink_Cards}
      </Cards>
    </div>
  );
}


export default Deck;
