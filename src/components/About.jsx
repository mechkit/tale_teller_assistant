import logo from '../TTA-logo.png';
import './About.css';

function About(props) {
  return (
    <div className='about'>
      <img src={logo} className="App-logo" alt="logo" />
      <h2>Tale Tellers Assistant</h2>
      <p>This app will help tale tellers and game master generate interesting content for their story.</p>
      <p>
        Powered by <a
          className="App-link"
          href="https://gitlab.com/mechkit/random_rpg_generator"
          target="_blank"
          rel="noopener noreferrer"
        >RRPGG</a>
      </p>
    </div>
  );
}

export default About;
