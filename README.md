# Tale Tellers Assistant

![TTA logo](./src/TTA-logo.svg)


## About

This app generates details about a fictional world to assist a tale teller or game master.

Demo: [taleteller.app](https://taleteller.app)

## Sub-projects:

- [Random RPG Generator](https://gitlab.com/mechkit/random_rpg_generator) - A story tellers assistant
